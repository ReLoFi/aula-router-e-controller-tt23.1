const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");


const Address = sequelize.define("Address", {
    state: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    street: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    number: {
      type: DataTypes.NUMBER,
      allowNull: false,
    }
  }, {timestamps: true});
  

Address.associate = (models) => {
  Address.belongsTo(models.User);
}

module.exports = Address;