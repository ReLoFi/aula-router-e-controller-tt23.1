const Express = require('express');
const router = Express();
const UserController = require('../controllers/UserController');
const AddressController = require('../controllers/AddressController');

//rotas para o usuário
router.post('/user', UserController.create);
router.get('/user/:id', UserController.show);
router.get('/user', UserController.index);
router.put('/user/:id', UserController.update);
router.delete('/user/:id', UserController.destroy);

//rotas para endereço
router.post('/address', AddressController.create);
router.get('/address/:id', AddressController.show);

//rota pra relação usuario-endereço
router.put('/user/:userId/address/:addressId', AddressController.addUser);



module.exports = router;