const Address = require('../models/Address');
const User = require('../models/User');

async function create(req,res) {
    try{
        const address = await Address.create(req.body);
        return res.status(200).json(address);
    }catch(error) {
        return res.status(500).json(error);
    }
};

async function show (req, res) {
    const {id} = req.params;
    try {
        const address = await Address.findByPk(id);
        return res.status(200).json(address);
    } catch (e) {
        return res.status(500).json(e)
    }
}

async function addUser (req, res) {
    const {userId, addressId} = req.params;
    try {
        const user = await User.findByPk(userId);
        const address = await Address.findByPk(addressId);
        await address.setUser(user);
        return res.status(200).json(address);
    } catch (error) {
        return res.status(500).json({error});
    }
}

module.exports = {
    create,
    show,
    addUser,
}
